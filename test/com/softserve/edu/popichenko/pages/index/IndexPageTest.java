package com.softserve.edu.popichenko.pages.index;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.util.concurrent.TimeUnit;

/**
 * Created by Eugene_P on 24-Jan-16.
 */
public class IndexPageTest {

    WebDriver driver;
    IndexPage indexPage;

    @BeforeMethod
    public void setUp() throws Exception {
        driver = new FirefoxDriver();
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        driver.get("http://comments.azurewebsites.net/");
    }

//    @AfterMethod
//    public void tearDown() throws Exception {
//        try {
//            driver.quit();
//        } catch (Exception e) {
//            // ignore
//        }
//    }

    @Test
    public void dummy() {

        indexPage = new IndexPage(driver);
        indexPage.clickNewButton();


    }
}