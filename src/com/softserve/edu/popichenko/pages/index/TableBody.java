package com.softserve.edu.popichenko.pages.index;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.FindBys;
import org.openqa.selenium.support.PageFactory;

import java.util.List;
import java.util.Map;

/**
 * Created by Eugene_P on 24-Jan-16.
 */
public class TableBody {
    private WebDriver driver;

    @FindBys({
            @FindBy(xpath = ".//*[@id='main']/div/div[5]/form/table/tbody/tr")
    })
    List<WebElement> tableRow; //TODO: List<WebElement> tableRow == @FindBy(xpath = ".//*[@id='main']/div/div[5]/form/table/tbody/tr[2]/td[1]") + CommentClass.getAllValues()

    Map<WebElement, WebElement> table;


    public TableBody(WebDriver driver) {
        this.driver = driver;

        //This initElements method will create all WebElements
        PageFactory.initElements(driver, this);

        initTable();
    }

    private void initTable() {
        for (int i = 0; i < tableRow.size(); i++) {

            int numOfColumns = 5;
            for (int j = 0; j < numOfColumns; j++) {
                table.put(tableRow.get(i), tableRow.get(i).findElement(By.xpath(".//td[" + j + "]")));
            }

        }
    }
}
